package br.edu.unisep.hostel.data.entity.listing;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "listing_type")
public class ListingType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private Integer id;

    @Column(name = "type_name")
    private String name;

}
