package br.edu.unisep.hostel.data.entity.listing;

import br.edu.unisep.base.data.entity.User;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "listing")
public class Listing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "listing_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Double price;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "type_id")
    private ListingType type;

    @Column(name = "status")
    private Integer status;

}
