package br.edu.unisep.hostel.data.repository.booking;

import br.edu.unisep.hostel.data.entity.booking.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {

    @Query("from Booking where user.id = :userId order by startDate desc")
    List<Booking> findByUser(Integer userId);

}
