package br.edu.unisep.hostel.data.repository.listing;

import br.edu.unisep.hostel.data.entity.listing.Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingRepository extends JpaRepository<Listing, Integer> {

    @Query("from Listing where user.id = :userId")
    List<Listing> findByUser(Integer userId);

    @Query("from Listing where user.id <> :userId")
    List<Listing> findByOtherUsers(Integer userId);

}
