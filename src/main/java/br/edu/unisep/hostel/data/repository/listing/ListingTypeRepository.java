package br.edu.unisep.hostel.data.repository.listing;

import br.edu.unisep.hostel.data.entity.listing.ListingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListingTypeRepository extends JpaRepository<ListingType, Integer> {
}
