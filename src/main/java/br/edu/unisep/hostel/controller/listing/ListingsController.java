package br.edu.unisep.hostel.controller.listing;

import br.edu.unisep.hostel.domain.dto.listing.ListingDto;
import br.edu.unisep.hostel.domain.dto.listing.RegisterListingDto;
import br.edu.unisep.hostel.domain.usecase.listing.FindListingsByOtherUsersUseCase;
import br.edu.unisep.hostel.domain.usecase.listing.FindListingsByUserUseCase;
import br.edu.unisep.hostel.domain.usecase.listing.RegisterListingUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/listings")
@AllArgsConstructor
public class ListingsController {

    private final FindListingsByUserUseCase findListingsByUserUseCase;
    private final FindListingsByOtherUsersUseCase findListingsByOtherUsersUseCase;
    private final RegisterListingUseCase registerListingUseCase;

    @RolesAllowed("ROLE_CLIENT")
    @GetMapping
    public ResponseEntity<List<ListingDto>> findMyListings(@RequestAttribute("tokenUserId") Integer userId) {
        var listings = findListingsByUserUseCase.execute(userId);
        return ResponseEntity.ok(listings);
    }

    @RolesAllowed("ROLE_ADMIN")
    @GetMapping("/by-user/{userId}")
    public ResponseEntity<List<ListingDto>> findByUser(@PathVariable("userId") Integer userId) {
        var listings = findListingsByUserUseCase.execute(userId);
        return ResponseEntity.ok(listings);
    }

    @RolesAllowed("ROLE_CLIENT")
    @GetMapping("/by-others")
    public ResponseEntity<List<ListingDto>> findByOtherUsers(@RequestAttribute("tokenUserId") Integer userId) {
        var listings = findListingsByOtherUsersUseCase.execute(userId);
        return ResponseEntity.ok(listings);
    }

    @RolesAllowed("ROLE_CLIENT")
    @PostMapping
    public ResponseEntity save(@RequestAttribute("tokenUserId") Integer userId,
                               @RequestBody RegisterListingDto registerListing) {
        registerListingUseCase.execute(registerListing, userId);
        return ResponseEntity.ok().build();
    }

}
