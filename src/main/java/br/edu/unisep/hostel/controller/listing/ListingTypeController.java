package br.edu.unisep.hostel.controller.listing;

import br.edu.unisep.hostel.domain.dto.listing.ListingTypeDto;
import br.edu.unisep.hostel.domain.usecase.listing.FindAllListingTypesUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/listing-type")
@AllArgsConstructor
public class ListingTypeController {

    private final FindAllListingTypesUseCase findAllListingTypesUseCase;

    @GetMapping
    public ResponseEntity<List<ListingTypeDto>> findAll() {
        var listingTypes = findAllListingTypesUseCase.execute();
        return ResponseEntity.ok(listingTypes);
    }
}
