package br.edu.unisep.hostel.controller.booking;

import br.edu.unisep.hostel.domain.dto.booking.BookingDto;
import br.edu.unisep.hostel.domain.dto.booking.RegisterBookingDto;
import br.edu.unisep.hostel.domain.usecase.booking.FindBookingsByUserUseCase;
import br.edu.unisep.hostel.domain.usecase.booking.RegisterBookingUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/bookings")
@AllArgsConstructor
public class BookingsController {

    private final FindBookingsByUserUseCase findBookingsByUserUseCase;
    private final RegisterBookingUseCase registerBookingUseCase;

    @RolesAllowed("ROLE_CLIENT")
    @GetMapping
    public ResponseEntity<List<BookingDto>> findMyBookings(@RequestAttribute("tokenUserId") Integer userId) {
        var listings = findBookingsByUserUseCase.execute(userId);
        return ResponseEntity.ok(listings);
    }

    @RolesAllowed("ROLE_ADMIN")
    @GetMapping("/by-user/{userId}")
    public ResponseEntity<List<BookingDto>> findByUser(@PathVariable("userId") Integer userId) {
        var listings = findBookingsByUserUseCase.execute(userId);
        return ResponseEntity.ok(listings);
    }

    @RolesAllowed("ROLE_CLIENT")
    @PostMapping
    public ResponseEntity save(@RequestAttribute("tokenUserId") Integer userId,
                               @RequestBody RegisterBookingDto registerListing) {
        registerBookingUseCase.execute(registerListing, userId);
        return ResponseEntity.ok().build();
    }

}
