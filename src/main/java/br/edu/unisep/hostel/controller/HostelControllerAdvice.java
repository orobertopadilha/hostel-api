package br.edu.unisep.hostel.controller;

import br.edu.unisep.hostel.domain.dto.error.ResponseErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class HostelControllerAdvice {

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
    public ResponseEntity<ResponseErrorDto> handleValidationErrors(Exception exception) {
        return ResponseEntity.badRequest().body(new ResponseErrorDto(exception.getMessage()));
    }

}
