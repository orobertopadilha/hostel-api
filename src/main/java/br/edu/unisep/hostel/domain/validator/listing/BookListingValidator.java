package br.edu.unisep.hostel.domain.validator.listing;

import br.edu.unisep.hostel.data.entity.listing.Listing;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static br.edu.unisep.hostel.domain.validator.ValidationMessages.MESSAGE_INVALID_LISTING;
import static br.edu.unisep.hostel.domain.validator.ValidationMessages.MESSAGE_LISTING_ALREADY_BOOKED;

@Component
public class BookListingValidator {

    public void validate(Optional<Listing> optionListing) {
        if (optionListing.isEmpty()) {
            throw new IllegalArgumentException(MESSAGE_INVALID_LISTING);
        }

        var listing = optionListing.get();
        if (!listing.getStatus().equals(ListingBuilder.ID_STATUS_AVAILABLE)) {
            throw new IllegalArgumentException(MESSAGE_LISTING_ALREADY_BOOKED);
        }
    }

}
