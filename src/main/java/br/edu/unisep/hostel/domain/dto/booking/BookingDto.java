package br.edu.unisep.hostel.domain.dto.booking;

import br.edu.unisep.hostel.domain.dto.listing.ListingDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class BookingDto {

    private final Integer id;
    private final String user;

    private final ListingDto listing;

    private final LocalDate startDate;
    private final LocalDate endDate;

    private final Double price;
    private final Double total;

}
