package br.edu.unisep.hostel.domain.usecase.listing;

import br.edu.unisep.hostel.data.repository.listing.ListingRepository;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import br.edu.unisep.hostel.domain.dto.listing.RegisterListingDto;
import br.edu.unisep.hostel.domain.validator.listing.RegisterListingValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterListingUseCase {

    private final RegisterListingValidator registerListingValidator;
    private final ListingRepository listingRepository;
    private final ListingBuilder listingBuilder;

    public void execute(RegisterListingDto registerListing, Integer userId) {
        registerListingValidator.validate(registerListing);

        var listing = listingBuilder.from(registerListing, userId);
        listingRepository.save(listing);
    }

}
