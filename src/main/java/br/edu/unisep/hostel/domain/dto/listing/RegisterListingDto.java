package br.edu.unisep.hostel.domain.dto.listing;

import lombok.Data;

@Data
public class RegisterListingDto {

    private final String title;
    private final String description;

    private final Double price;

    private final Integer type;

}
