package br.edu.unisep.hostel.domain.builder.listing;

import br.edu.unisep.base.data.entity.User;
import br.edu.unisep.hostel.data.entity.listing.Listing;
import br.edu.unisep.hostel.data.entity.listing.ListingType;
import br.edu.unisep.hostel.domain.dto.listing.ListingDto;
import br.edu.unisep.hostel.domain.dto.listing.RegisterListingDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ListingBuilder {

    public static final String STATUS_AVAILABLE = "Disponível";
    public static final String STATUS_BOOKED = "Alugado";
    public static final String STATUS_INACTIVE = "Inativo";

    public static final Integer ID_STATUS_AVAILABLE = 1;
    public static final Integer ID_STATUS_BOOKED = 2;

    public List<ListingDto> from(List<Listing> listings) {
        return listings.stream().map(this::from).collect(Collectors.toList());
    }

    public ListingDto from(Listing listing) {
        return new ListingDto(
                listing.getId(),
                listing.getTitle(),
                listing.getDescription(),
                listing.getPrice(),
                listing.getUser().getName(),
                listing.getType().getName(),
                mapStatus(listing.getStatus())
        );
    }

    public Listing from(RegisterListingDto registerListing, Integer userId) {
        var listing = new Listing();
        listing.setTitle(registerListing.getTitle());
        listing.setDescription(registerListing.getDescription());
        listing.setPrice(registerListing.getPrice());
        listing.setStatus(ID_STATUS_AVAILABLE);

        var listingType = new ListingType();
        listingType.setId(registerListing.getType());
        listing.setType(listingType);

        var user = new User();
        user.setId(userId);
        listing.setUser(user);

        return listing;
    }

    private String mapStatus(Integer status) {
        if (status.equals(ID_STATUS_AVAILABLE)) {
            return STATUS_AVAILABLE;
        } else if (status.equals(ID_STATUS_BOOKED)) {
            return STATUS_BOOKED;
        }

        return STATUS_INACTIVE;
    }

}
