package br.edu.unisep.hostel.domain.dto.listing;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ListingTypeDto {

    private final Integer id;

    private final String name;

}
