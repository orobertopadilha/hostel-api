package br.edu.unisep.hostel.domain.validator.booking;

import br.edu.unisep.hostel.domain.dto.booking.RegisterBookingDto;
import org.springframework.stereotype.Component;

import static br.edu.unisep.hostel.domain.validator.ValidationMessages.*;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

@Component
public class RegisterBookingValidator {

    public void validate(RegisterBookingDto registerBooking) {
        notNull(registerBooking.getListing(), MESSAGE_INVALID_LISTING);
        notNull(registerBooking.getStartDate(), MESSAGE_INVALID_START_DATE);
        notNull(registerBooking.getEndDate(), MESSAGE_INVALID_END_DATE);

        isTrue(registerBooking.getEndDate().isAfter(registerBooking.getStartDate()), MESSAGE_INVALID_PERIOD);

    }

}
