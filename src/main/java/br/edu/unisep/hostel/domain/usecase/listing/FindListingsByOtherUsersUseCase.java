package br.edu.unisep.hostel.domain.usecase.listing;

import br.edu.unisep.hostel.data.repository.listing.ListingRepository;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import br.edu.unisep.hostel.domain.dto.listing.ListingDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindListingsByOtherUsersUseCase {

    private final ListingRepository listingRepository;
    private final ListingBuilder listingBuilder;

    public List<ListingDto> execute(Integer userId) {
        var listings = listingRepository.findByOtherUsers(userId);
        return listingBuilder.from(listings);
    }
}
