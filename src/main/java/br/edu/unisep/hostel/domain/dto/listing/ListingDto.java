package br.edu.unisep.hostel.domain.dto.listing;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ListingDto {

    private final Integer id;

    private final String title;
    private final String description;

    private final Double price;

    private final String user;
    private final String type;
    private final String status;

}
