package br.edu.unisep.hostel.domain.usecase.booking;

import br.edu.unisep.hostel.data.repository.booking.BookingRepository;
import br.edu.unisep.hostel.domain.builder.booking.BookingBuilder;
import br.edu.unisep.hostel.domain.dto.booking.BookingDto;
import br.edu.unisep.hostel.domain.validator.user.UserIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindBookingsByUserUseCase {

    private final UserIdValidator userIdValidator;
    private final BookingBuilder bookingBuilder;
    private final BookingRepository bookingRepository;

    public List<BookingDto> execute(Integer userId) {
        userIdValidator.validate(userId);

        var bookings = bookingRepository.findByUser(userId);

        return bookingBuilder.from(bookings);
    }

}
