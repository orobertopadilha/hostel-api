package br.edu.unisep.hostel.domain.usecase.listing;

import br.edu.unisep.hostel.data.repository.listing.ListingTypeRepository;
import br.edu.unisep.hostel.domain.builder.listing.ListingTypeBuilder;
import br.edu.unisep.hostel.domain.dto.listing.ListingTypeDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllListingTypesUseCase {

    private final ListingTypeRepository listingTypeRepository;
    private final ListingTypeBuilder listingTypeBuilder;

    public List<ListingTypeDto> execute() {
        var listingTypes = listingTypeRepository.findAll();
        return listingTypeBuilder.from(listingTypes);
    }

}
