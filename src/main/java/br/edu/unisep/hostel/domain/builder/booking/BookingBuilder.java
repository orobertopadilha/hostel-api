package br.edu.unisep.hostel.domain.builder.booking;

import br.edu.unisep.base.data.entity.User;
import br.edu.unisep.hostel.data.entity.booking.Booking;
import br.edu.unisep.hostel.data.entity.listing.Listing;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import br.edu.unisep.hostel.domain.dto.booking.BookingDto;
import br.edu.unisep.hostel.domain.dto.booking.RegisterBookingDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class BookingBuilder {

    private final ListingBuilder listingBuilder;

    public List<BookingDto> from(List<Booking> bookings) {
        return bookings.stream().map(this::from).collect(Collectors.toList());
    }

    private BookingDto from(Booking booking) {
        var period = Period.between(booking.getStartDate(), booking.getEndDate()).getDays();
        var total = period * booking.getPrice();

        return new BookingDto(
                booking.getId(),
                booking.getUser().getName(),
                listingBuilder.from(booking.getListing()),
                booking.getStartDate(),
                booking.getEndDate(),
                booking.getPrice(),
                total
        );
    }

    public Booking from(RegisterBookingDto registerBooking, Listing listing, Integer userId) {
        var booking = new Booking();

        booking.setListing(listing);
        booking.setStartDate(registerBooking.getStartDate());
        booking.setEndDate(registerBooking.getEndDate());
        booking.setPrice(listing.getPrice());

        var user = new User();
        user.setId(userId);

        booking.setUser(user);

        return booking;
    }

}
