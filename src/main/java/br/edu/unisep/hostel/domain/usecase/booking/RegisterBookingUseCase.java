package br.edu.unisep.hostel.domain.usecase.booking;

import br.edu.unisep.hostel.data.repository.booking.BookingRepository;
import br.edu.unisep.hostel.data.repository.listing.ListingRepository;
import br.edu.unisep.hostel.domain.builder.booking.BookingBuilder;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import br.edu.unisep.hostel.domain.dto.booking.RegisterBookingDto;
import br.edu.unisep.hostel.domain.validator.booking.RegisterBookingValidator;
import br.edu.unisep.hostel.domain.validator.listing.BookListingValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static br.edu.unisep.hostel.domain.validator.ValidationMessages.MESSAGE_INVALID_LISTING;

@Service
@AllArgsConstructor
public class RegisterBookingUseCase {

    private final RegisterBookingValidator registerBookingValidator;
    private final BookListingValidator bookListingValidator;

    private final BookingBuilder bookingBuilder;
    private final BookingRepository bookingRepository;
    private final ListingRepository listingRepository;

    @Transactional
    public void execute(RegisterBookingDto registerBooking, Integer userId) {
        registerBookingValidator.validate(registerBooking);

        var optListing = listingRepository.findById(registerBooking.getListing());
        bookListingValidator.validate(optListing);

        var listing = optListing.get();

        var booking = bookingBuilder.from(registerBooking, listing, userId);
        bookingRepository.save(booking);

        listing.setStatus(ListingBuilder.ID_STATUS_BOOKED);
        listingRepository.save(listing);
    }
}
