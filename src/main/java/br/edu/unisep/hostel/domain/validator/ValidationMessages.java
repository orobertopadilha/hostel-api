package br.edu.unisep.hostel.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    public static final String MESSAGE_INVALID_USER_ID = "Usuário inválido!";

    public static final String MESSAGE_INVALID_TITLE = "Título inválido!";
    public static final String MESSAGE_INVALID_DESCRIPTION = "Descrição inválida!";
    public static final String MESSAGE_INVALID_PRICE = "Preço inválido!";
    public static final String MESSAGE_INVALID_TYPE = "Tipo de anúncio inválido!";

    public static final String MESSAGE_INVALID_LISTING = "Id do anúncio inválido!";
    public static final String MESSAGE_LISTING_ALREADY_BOOKED = "Anúncio já reservado!";
    public static final String MESSAGE_INVALID_START_DATE = "Data de início inválida!";
    public static final String MESSAGE_INVALID_END_DATE = "Data de término inválida!";
    public static final String MESSAGE_INVALID_PERIOD = "Período de reserva inválido!";

}
