package br.edu.unisep.hostel.domain.validator.listing;

import br.edu.unisep.hostel.domain.dto.listing.RegisterListingDto;
import org.springframework.stereotype.Component;

import static br.edu.unisep.hostel.domain.validator.ValidationMessages.*;
import static org.apache.commons.lang3.Validate.*;

@Component
public class RegisterListingValidator {

    public void validate(RegisterListingDto registerListing) {
        notEmpty(registerListing.getTitle(), MESSAGE_INVALID_TITLE);
        notEmpty(registerListing.getDescription(), MESSAGE_INVALID_DESCRIPTION);

        notNull(registerListing.getPrice(), MESSAGE_INVALID_PRICE);
        isTrue(registerListing.getPrice() > 0d, MESSAGE_INVALID_PRICE);

        notNull(registerListing.getType(), MESSAGE_INVALID_TYPE);
    }

}
