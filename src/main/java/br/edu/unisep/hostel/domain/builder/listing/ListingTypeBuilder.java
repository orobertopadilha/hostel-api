package br.edu.unisep.hostel.domain.builder.listing;

import br.edu.unisep.hostel.data.entity.listing.ListingType;
import br.edu.unisep.hostel.domain.dto.listing.ListingTypeDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ListingTypeBuilder {

    public List<ListingTypeDto> from(List<ListingType> types) {
        return types.stream().map(this::from).collect(Collectors.toList());
    }

    private ListingTypeDto from(ListingType listingType) {
        return new ListingTypeDto(
                listingType.getId(),
                listingType.getName()
        );
    }

}
