package br.edu.unisep.hostel.domain.dto.booking;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RegisterBookingDto {

    private Integer listing;

    private LocalDate startDate;
    private LocalDate endDate;

}
