package br.edu.unisep.hostel.domain.usecase.listing;

import br.edu.unisep.hostel.data.repository.listing.ListingRepository;
import br.edu.unisep.hostel.domain.builder.listing.ListingBuilder;
import br.edu.unisep.hostel.domain.dto.listing.ListingDto;
import br.edu.unisep.hostel.domain.validator.user.UserIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindListingsByUserUseCase {

    private final UserIdValidator userIdValidator;
    private final ListingRepository listingRepository;
    private final ListingBuilder listingBuilder;

    public List<ListingDto> execute(Integer userId) {
        userIdValidator.validate(userId);

        var listings = listingRepository.findByUser(userId);
        return listingBuilder.from(listings);
    }

}
